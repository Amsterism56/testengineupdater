﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré par un outil.
//     Version du runtime :4.0.30319.42000
//
//     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
//     le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

using System.Xml.Serialization;

// 
// Ce code source a été automatiquement généré par xsd, Version=4.8.3928.0.
// 


/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
[System.Xml.Serialization.XmlRootAttribute(Namespace="", IsNullable=false)]
public partial class testsuites {
    
    private testsuitesTestsuite[] itemsField;
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("testsuite", Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public testsuitesTestsuite[] Items {
        get {
            return this.itemsField;
        }
        set {
            this.itemsField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
public partial class testsuitesTestsuite {
    
    private testsuitesTestsuiteTestcase[] testcaseField;
    
    private string nameField;
    
    private string testsField;
    
    private string errorsField;
    
    private string failuresField;
    
    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("testcase", Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public testsuitesTestsuiteTestcase[] testcase {
        get {
            return this.testcaseField;
        }
        set {
            this.testcaseField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string name {
        get {
            return this.nameField;
        }
        set {
            this.nameField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string tests {
        get {
            return this.testsField;
        }
        set {
            this.testsField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string errors {
        get {
            return this.errorsField;
        }
        set {
            this.errorsField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string failures {
        get {
            return this.failuresField;
        }
        set {
            this.failuresField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
public partial class testsuitesTestsuiteTestcase {
    
    private string nameField;
    
    private string timeField;

    private testsuitesTestsuiteTestcaseFailure[] failureField;

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("failure", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
    public testsuitesTestsuiteTestcaseFailure[] failure
    {
        get
        {
            return this.failureField;
        }
        set
        {
            this.failureField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string name {
        get {
            return this.nameField;
        }
        set {
            this.nameField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string time {
        get {
            return this.timeField;
        }
        set {
            this.timeField = value;
        }
    }
}

/// <remarks/>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class testsuitesTestsuiteTestcaseFailure
{

    private string messageField;
    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string message
    {
        get
        {
            return this.messageField;
        }
        set
        {
            this.messageField = value;
        }
    }
}
