﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media;
using System.Xml.Serialization;

namespace TestEngineUpdater
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Public Members

        public string InputPath { get; set; }

        public string OutputPath { get; set; }

        public int MinTestsToUpdate { get; set; }

        public int OtherTestsToUpdate { get; set; }

        public Dictionary<string, bool> ListOfTestsToUpdate { get; set; }

        public ShowDetails ChildWindow { get; set; }

        public bool IsCopySuccess { get; set; }

        public bool IsOtherFolder { get; set; }

        public bool IsInitialized { get; set; }

        public bool IsDarkMode { get; set; }

        #endregion Public Members

        #region Constructor

        public MainWindow()
        {
            IsInitialized = false;
            InitializeComponent();
            IsInitialized = true;
        }

        #endregion Constructor

        #region User events

        private void OnMinTestUpdateButtonClick(object sender, RoutedEventArgs e)
        {
            _snackbarSuccess.IsActive = false;

            if (MinTestsToUpdate == 0)
            {
                System.Windows.Forms.MessageBox.Show("There are no min tests to update", "No min tests to update", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (OutputPath == null)
            {
                System.Windows.Forms.MessageBox.Show("Output folder is not selected", "No output folder", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                IsCopySuccess = true;

                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                worker.DoWork += UpdateTests;
                worker.ProgressChanged += Worker_ProgressChanged;
                worker.RunWorkerCompleted += Worker_RunWorkerCompleted;

                worker.RunWorkerAsync();
            }
        }

        private void OnOtherTestUpdateButtonClick(object sender, RoutedEventArgs e)
        {
            _snackbarSuccess.IsActive = false;

            if (OtherTestsToUpdate == 0)
            {
                System.Windows.Forms.MessageBox.Show("There are no other tests to update", "No other tests to update", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (OutputPath == null)
            {
                System.Windows.Forms.MessageBox.Show("Output folder is not selected", "No output folder", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                IsCopySuccess = true;

                BackgroundWorker worker = new BackgroundWorker();
                worker.WorkerReportsProgress = true;
                worker.DoWork += UpdateTests;
                worker.ProgressChanged += Worker_ProgressChanged;
                worker.RunWorkerCompleted += Worker_RunWorkerCompleted;

                worker.RunWorkerAsync();
            }
        }

        private void OnSwitchColorModeUnchecked(object sender, RoutedEventArgs e)
        {
            //light mode
            IsDarkMode = false;
            _mainWindow.Background = new SolidColorBrush(Color.FromRgb(240, 240, 240));
            
            _leftPanel.Background = new SolidColorBrush(Color.FromRgb(0, 98, 195));
            _minToUpdateLabel.Foreground = new SolidColorBrush(Colors.White);
            _inputFolder.Background = new SolidColorBrush(Color.FromRgb(0, 116, 232));
            _minToUpdate.Background = new SolidColorBrush(Color.FromRgb(110, 156, 255));
            _minToUpdate.BorderBrush = new SolidColorBrush(Color.FromRgb(110, 156, 255));
            _outputFolder.Background = new SolidColorBrush(Color.FromRgb(0, 116, 232));
            _othersToUpdate.Background = new SolidColorBrush(Color.FromRgb(110, 156, 255));
            _othersToUpdate.BorderBrush = new SolidColorBrush(Color.FromRgb(110, 156, 255));
            _updateMinTestsButton.Background = new SolidColorBrush(Color.FromRgb(0, 116, 232));
            _updateOtherTestButton.Background = new SolidColorBrush(Color.FromRgb(0, 116, 232));
            _showDetails.Background = new SolidColorBrush(Color.FromRgb(0, 116, 232));
            _othersToUpdateLabel.Foreground = new SolidColorBrush(Colors.White);

            _minProgressBar.Background = new SolidColorBrush(Color.FromRgb(110, 156, 255));
            _minProgressBar.BorderBrush = new SolidColorBrush(Color.FromRgb(110, 156, 255));
            _minProgressBar.Foreground = new SolidColorBrush(Color.FromRgb(0, 116, 232));

            _othersProgressBar.Background = new SolidColorBrush(Color.FromRgb(110, 156, 255));
            _othersProgressBar.BorderBrush = new SolidColorBrush(Color.FromRgb(110, 156, 255));
            _othersProgressBar.Foreground = new SolidColorBrush(Color.FromRgb(0, 116, 232));

            _SwitchColorMode.Background = new SolidColorBrush(Colors.White);
            _SwitchColorMode.BorderBrush = new SolidColorBrush(Color.FromRgb(112, 112, 112));
            _SwitchColorMode.Foreground = new SolidColorBrush(Color.FromRgb(0, 116, 232));
        }

        private void OnSwitchColorModeChecked(object sender, RoutedEventArgs e)
        {
            IsDarkMode = true;
            if (IsInitialized)
            {
                //Dark mode
                _mainWindow.Background = new LinearGradientBrush(new GradientStopCollection()
            {new GradientStop()
                {
                    Color = Color.FromRgb(44,45,46),
                    Offset = 0
                },
            new GradientStop()
                {
                    Color = Color.FromRgb(113,68,193),
                    Offset = 1
                }
            });
                _leftPanel.Background = new SolidColorBrush(Color.FromRgb(44, 45, 46));
                _minToUpdateLabel.Foreground = new SolidColorBrush(Colors.White);
                _minToUpdate.Background = new SolidColorBrush(Color.FromRgb(179, 157, 219));
                _minToUpdate.BorderBrush = new SolidColorBrush(Color.FromRgb(179, 157, 219));
                _inputFolder.Background = new SolidColorBrush(Color.FromRgb(103, 58, 183));
                _outputFolder.Background = new SolidColorBrush(Color.FromRgb(103, 58, 183));
                _othersToUpdate.Background = new SolidColorBrush(Color.FromRgb(179, 157, 219));
                _othersToUpdate.BorderBrush = new SolidColorBrush(Color.FromRgb(179, 157, 219));
                _showDetails.Background = new SolidColorBrush(Color.FromRgb(103, 58, 183));
                _othersToUpdateLabel.Foreground = new SolidColorBrush(Colors.White);
                _updateMinTestsButton.Background = new SolidColorBrush(Color.FromRgb(103, 58, 183));
                _updateOtherTestButton.Background = new SolidColorBrush(Color.FromRgb(103, 58, 183));

                _minProgressBar.Background = new SolidColorBrush(Color.FromRgb(179, 157, 219));
                _minProgressBar.BorderBrush = new SolidColorBrush(Color.FromRgb(179, 157, 219));
                _minProgressBar.Foreground = new SolidColorBrush(Color.FromRgb(103, 58, 183));

                _othersProgressBar.Background = new SolidColorBrush(Color.FromRgb(179, 157, 219));
                _othersProgressBar.BorderBrush = new SolidColorBrush(Color.FromRgb(179, 157, 219));
                _othersProgressBar.Foreground = new SolidColorBrush(Color.FromRgb(103, 58, 183));

                _SwitchColorMode.Background = new SolidColorBrush(Color.FromRgb(103, 58, 183));
                _SwitchColorMode.BorderBrush = new SolidColorBrush(Color.FromRgb(112, 112, 112));
                _SwitchColorMode.Foreground = new SolidColorBrush(Colors.White);
            }
        }

        private void OnInputFolderButtonClick(object sender, RoutedEventArgs e)
        {
            using (var dialog = new FolderBrowserDialog())
            {
                dialog.SelectedPath = @"D:\git";
                dialog.Description = "Selectionnez le dossier d'entrée engineXmlTests";
                if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    ListOfTestsToUpdate = new Dictionary<string, bool>();

                    InputPath = dialog.SelectedPath;

                    //Look for _JUnitResults folder
                    var jUnitFolder = Directory.GetDirectories(InputPath, "_JUnitResults", SearchOption.AllDirectories);
                    if (jUnitFolder.Length == 0)
                    {
                        System.Windows.Forms.MessageBox.Show("No folder named _JUnitResults found in this location", "Folder not found", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        InputPath = null;
                        _inputFolder.Content = "Select input folder";
                        _inputFolder.ToolTip = null;
                    }
                    else if (jUnitFolder.Length > 1)
                    {
                        System.Windows.Forms.MessageBox.Show("Several folders named _JUnitResults found in this location. Please be more specific", "Several folders found", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        InputPath = null;
                        _inputFolder.Content = "Select input folder";
                        _inputFolder.ToolTip = null;
                    }
                    else
                    {
                        InputPath = jUnitFolder[0];
                        _inputFolder.ToolTip = Directory.GetParent(InputPath).FullName;
                        if (Directory.GetParent(InputPath).FullName.Length > 20)
                        {
                            _inputFolder.Content = Directory.GetParent(InputPath).FullName.Substring(0, 20) + "...";
                        }
                        else
                        {
                            _inputFolder.Content = Directory.GetParent(InputPath).FullName;
                        }
                    }

                    if (InputPath != null)
                    {
                        IsOtherFolder = false;
                        int testsToUpdate = 0;
                        var testFiles = Directory.GetFiles(InputPath);
                        InputPath = Directory.GetParent(InputPath).FullName;

                        if (System.IO.Path.GetFileName(testFiles[0]).Contains("_other"))
                        {
                            IsOtherFolder = true;
                        }

                        var serializer = new XmlSerializer(typeof(testsuites));

                        foreach (var file in testFiles)
                        {
                            //Deserialize file
                            TextReader reader = new StringReader(File.ReadAllText(file));
                            var testSuites = (testsuites)serializer.Deserialize(reader);

                            //Get number of errors/failures
                            int nbFails = Int32.Parse(testSuites.Items[0].failures);
                            testsToUpdate += nbFails;

                            //Identify fails tests
                            if (nbFails > 0)
                            {
                                foreach (var fail in testSuites.Items[0].testcase)
                                {
                                    if (fail.failure != null)
                                    {
                                        string[] elementPath = testSuites.Items[0].name.Split('_');
                                        string path = string.Empty;

                                        for (int i = 0; i < elementPath.Length; i++)
                                        {
                                            path = Path.Combine(path, elementPath[i]);
                                        }

                                        path = Path.Combine("engineXmlTests", path);

                                        if (IsOtherFolder)
                                        {
                                            path = Path.Combine(path, "other\\out", fail.name + ".xml");
                                        }
                                        else
                                        {
                                            path = Path.Combine(path, "min\\out", fail.name + ".xml");
                                        }

                                        ListOfTestsToUpdate.Add(path, true);
                                    }
                                }
                            }
                        }
                        if (IsOtherFolder)
                        {
                            _othersToUpdate.Content = testsToUpdate.ToString();
                            OtherTestsToUpdate = testsToUpdate;
                            _minToUpdate.Content = "0";
                            MinTestsToUpdate = 0;
                        }
                        else
                        {
                            _minToUpdate.Content = testsToUpdate.ToString();
                            MinTestsToUpdate = testsToUpdate;
                            _othersToUpdate.Content = "0";
                            OtherTestsToUpdate = 0;
                        }
                    }
                }
            }
        }

        private void OnOutputFolderButtonClick(object sender, RoutedEventArgs e)
        {
            using (var dialog = new FolderBrowserDialog())
            {
                dialog.SelectedPath = @"D:\git";
                dialog.Description = "Selectionnez le dossier de développement où remplacer les résultats";
                if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    var engineXmlTestsFolder = Directory.GetDirectories(dialog.SelectedPath, "engineXmlTests", SearchOption.AllDirectories);

                    if (engineXmlTestsFolder.Length == 0)
                    {
                        System.Windows.Forms.MessageBox.Show("No folder named engineXmlTests found in this location", "Folder not found", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        OutputPath = null;
                        _outputFolder.Content = "Select output folder";
                        _outputFolder.ToolTip = null;
                    }
                    else if (engineXmlTestsFolder.Length > 1)
                    {
                        System.Windows.Forms.MessageBox.Show("Several folders named engineXmlTests found in this location. Please be more specific", "Several folders found", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        OutputPath = null;
                        _outputFolder.Content = "Select output folder";
                        _outputFolder.ToolTip = null;
                    }
                    else
                    {
                        if (engineXmlTestsFolder[0].Length > 20)
                        {
                            _outputFolder.Content = engineXmlTestsFolder[0].Substring(0, 20) + "...";
                        }
                        else
                        {
                            _outputFolder.Content = engineXmlTestsFolder[0];
                        }

                        _outputFolder.ToolTip = engineXmlTestsFolder[0];
                        OutputPath = engineXmlTestsFolder[0];
                    }
                }
            }
        }

        private void OnShowDetailsButtonClick(object sender, RoutedEventArgs e)
        {
            if (ListOfTestsToUpdate == null)
            {
                System.Windows.Forms.MessageBox.Show("Please select input folder", "Input folder not selected", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (ListOfTestsToUpdate.Count == 0)
            {
                System.Windows.Forms.MessageBox.Show("There is no test to update in your selected folder", "No tests to update", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                this.IsEnabled = false;
                ChildWindow = new ShowDetails(this, ListOfTestsToUpdate, IsDarkMode);
                ChildWindow.Show();
            }
        }

        #endregion User events

        #region Private Methods

        private void UpdateTests(object sender, DoWorkEventArgs e)
        {
            (sender as BackgroundWorker).ReportProgress(0);
            for (int i = 1; i < ListOfTestsToUpdate.Count(x => x.Value == true) + 1; i++)
            {
                //Update test
                try
                {
                    foreach (var test in ListOfTestsToUpdate.Where(u => u.Value == true))
                    {
                        var destinationFile = System.IO.Path.Combine(Directory.GetParent(OutputPath).FullName, test.Key).Replace("\\out\\", "\\ref\\");
                        var referenceFile = System.IO.Path.Combine(Directory.GetParent(InputPath).FullName, test.Key);
                        if (File.Exists(destinationFile) && File.Exists(referenceFile))
                        {
                            //Copy xml file
                            File.Copy(referenceFile, destinationFile, true);

                            //Optionnal copy of .cov results
                            if (Directory.Exists(destinationFile.Replace(".xml",".cov")))
                            {
                                CopyDirectory(referenceFile.Replace(".xml", ".cov"), destinationFile.Replace(".xml", ".cov"));
                            }

                            //Update progress bar
                            int percent = 100 * i / ListOfTestsToUpdate.Count(x => x.Value == true);
                            (sender as BackgroundWorker).ReportProgress(percent);
                        }
                    }
                }
                catch (Exception ex)
                {
                    IsCopySuccess = false;
                }
            }
        }

        public void UpdateTestStates(Dictionary<string, bool> tests)
        {
            ListOfTestsToUpdate = tests;

            if (IsOtherFolder)
            {
                OtherTestsToUpdate = tests.Count(x => x.Value == true);
                _othersToUpdate.Content = OtherTestsToUpdate;
            }
            else
            {
                MinTestsToUpdate = tests.Count(x => x.Value == true);
                _minToUpdate.Content = MinTestsToUpdate;
            }
        }

        private void CloseChildrens(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (ChildWindow != null)
            {
                ChildWindow.Close();
            }
        }

        private void Worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (IsOtherFolder)
            {
                _othersProgressBar.Value = e.ProgressPercentage;
            }
            else
            {
                _minProgressBar.Value = e.ProgressPercentage;
            }
        }

        private void Worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            string message = "";

            if (IsCopySuccess)
            {
                message = "Les tests ont correctement été mis à jour";
                ListOfTestsToUpdate = new Dictionary<string, bool>();
                _inputFolder.Content = "Select input folder";
                InputPath = null;

                if (IsOtherFolder)
                {
                    OtherTestsToUpdate = 0;
                    _othersToUpdate.Content = 0;
                }
                else
                {
                    MinTestsToUpdate = 0;
                    _minToUpdate.Content = 0;
                }
            }
            else
            {
                message = "Erreur lors de la copie des fichiers";
            }

            _snackbarSuccess.MessageQueue?.Enqueue(
                message,
                null,
                null,
                null,
                false,
                true,
                TimeSpan.FromSeconds(5));
        }

        private void CopyDirectory(string referencePath, string destinationPath)
        {
            var diSource = new DirectoryInfo(referencePath);
            var diTarget = new DirectoryInfo(destinationPath);

            CopyAll(diSource, diTarget);
        }

        private void CopyAll(DirectoryInfo source, DirectoryInfo target)
        {
            Directory.CreateDirectory(target.FullName);

            // Copy each file into the new directory.
            foreach (FileInfo fi in source.GetFiles())
            {
                fi.CopyTo(Path.Combine(target.FullName, fi.Name), true);
            }

            // Copy each subdirectory using recursion.
            foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
            {
                DirectoryInfo nextTargetSubDir =
                    target.CreateSubdirectory(diSourceSubDir.Name);
                CopyAll(diSourceSubDir, nextTargetSubDir);
            }
        }

        #endregion Private Methods
    }
}