﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TestEngineUpdater
{
    /// <summary>
    /// Logique d'interaction pour ShowMore.xaml
    /// </summary>
    public partial class ShowDetails : Window
    {
        public MainWindow _parent;

        public Dictionary<string, bool> keyValues;
        public ShowDetails(MainWindow parent, Dictionary<string, bool> testToUpdate, bool isDarkMode)
        {
            _parent = parent;
            InitializeComponent();
            keyValues = testToUpdate;
            if (!isDarkMode)
            {
                _okButton.Background = new SolidColorBrush(Color.FromRgb(0, 116, 232));
                _okButton.BorderBrush = new SolidColorBrush(Color.FromRgb(0, 116, 232));
            }

            foreach (var test in testToUpdate)
            {
                var checkbox = new CheckBox();
                checkbox.IsChecked = test.Value;
                checkbox.Content = test.Key;
                checkbox.Click += new RoutedEventHandler(this.UpdateTestSelection);

                if (!isDarkMode)
                {
                    checkbox.Background = new SolidColorBrush(Color.FromRgb(0, 116, 232));
                }

                _mainListView.Items.Add(checkbox);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            _parent.IsEnabled = true;
            _parent.UpdateTestStates(keyValues);
            Close();
        }

        private void UpdateTestSelection(object sender, RoutedEventArgs e)
        {
            var checkbox = sender as CheckBox;
            keyValues[checkbox.Content.ToString()] = (Boolean)checkbox.IsChecked;
        }

        private void _showMore_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            _parent.IsEnabled = true;
        }
    }
}
